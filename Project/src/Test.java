
public class Test {
    private static final String encodedTextShanonFano = "000011001010111000";


    public static void main(String[] args) {
        testEncodeHamming();
    }

    private static void testEncodeHamming() {
        System.out.println("Original: " + encodedTextShanonFano);
        System.out.println("Encoded: " + Main.encodeHamming(encodedTextShanonFano));
    }
}
