import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/**
 * Done by: Zhetpisbayev Batyrzhan
 *          Masalkina Yuliya
 *          Tazhikenov Almat
 *          Janibekov Azamat
 * Group:   CSSE-1509r
 */

public class Main {

    static String filename = "Text.txt";

    private static boolean isFirstIter = true;

    private static Map<Character, StringBuilder> result = new LinkedHashMap<>();

    private static HashMap<Character, Integer> lettersMap = new HashMap<>();

    private static Map<Character, Double> map = new HashMap<>();

    public static void main(String[] args) {

        try {

            // Part 1 - getting char codes
            StringBuilder text = getCharCodes();
            ShannonFano(map);
            System.out.println("Char codes:\n");
            result.forEach((k, v) -> System.out.println(k + " " + v));

            // Part 2 - encoding text with Shanon-Fano
            String compressedText = text(text.toString(), result);
            System.out.println("\nEncoded with Shanon-Fano: \n" + compressedText);

            // Part 4 - encoding text with Hamming
            String encodedText = encodeHamming(compressedText);
            System.out.println("\nEncoded with Hamming: \n" + encodedText);

            // Part 5 - adding noise
            String textWithNoise = addNoise(encodedText);
            System.out.println("\nWith noise added: \n" + textWithNoise);

            // Part 6 - decoding with Hamming encoded text with noise added
            String decodedText = decodeHamming(textWithNoise);
            System.out.println("\nDecoded with Hamming: \n" + decodedText);

            // Part 3 - decoding with Shanon-Fano
            Map<String, Character> swapMap = swap(result);
            System.out.println("\nDecoded with Shanon-Fano: \n" + resultText(text(text.toString(), result), swapMap));

        } catch (IOException ex) {
            System.err.println("Text file not found!");
        }
    }

    public static StringBuilder getCharCodes() throws FileNotFoundException {
        StringBuilder text = new StringBuilder();
        Scanner sc = new Scanner(new File(filename));
        int textLength = 0;
        int breakLength = 0;

        // reading each line of text file
        while (sc.hasNext()) {
            text.append(sc.nextLine());
            breakLength += 1;    // counting line breaks
        }
        textLength = text.length() + breakLength;
        String origin = text.toString();

        // counting each symbol appearance in the text
        while (origin.length() != 0) {
            char letter = origin.charAt(0);
            int length = origin.length();
            origin = origin.replace(String.valueOf(letter), "");
            length -= origin.length();
            lettersMap.put(letter, length);
        }

        SortedSet<Character> keys = new TreeSet<>(lettersMap.keySet());

        // output
        // line breaks first
        map.put('\n', ((double) (breakLength - 1) / textLength));
        for (Character key : keys) {
            map.put(key, ((double) lettersMap.get(key) / textLength));
        }


        map = map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(value -> value.getKey(),
                        value -> value.getValue(),
                        (oldV, newV) -> newV,
                        LinkedHashMap::new));

        return text;
    }

    /**
     * Algorithm of Shannon Fano
     *
     * @param from
     */
    public static void ShannonFano(final Map<Character, Double> from) {
        if (from.size() == 1) {
            return;
        }
        final List<Character> one = new ArrayList<>();
        final List<Character> zero = new ArrayList<>(from.keySet());
        while (!zero.isEmpty()) {
            Character first = zero.get(0);
            zero.remove(0);
            one.add(first);
            double sum1 = zero.stream().mapToDouble(key -> from.get(key)).sum();
            double sum2 = one.stream().mapToDouble(key -> from.get(key)).sum();
            if (sum2 >= sum1) {
                if (isFirstIter) {
                    one.forEach(k -> result.putIfAbsent(k, new StringBuilder("1")));
                    zero.forEach(k -> result.putIfAbsent(k, new StringBuilder("0")));
                    isFirstIter = false;
                    break;
                }
                one.forEach(k -> result.put(k, result.get(k).append("1")));
                zero.forEach(k -> result.put(k, result.get(k).append("0")));
                break;
            }
        }

        ShannonFano(one.stream()
                .collect(toMap(k -> k,
                        k -> from.get(k),
                        (oldV, newV) -> newV,
                        LinkedHashMap::new)));

        ShannonFano(zero.stream()
                .collect(toMap(k -> k,
                        k -> from.get(k),
                        (oldV, newV) -> newV,
                        LinkedHashMap::new)));
    }

    /**
     * @param text
     * @param map
     * @return
     */
    private static String text(String text, Map<Character, StringBuilder> map) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            builder.append(map.get(text.charAt(i)));
        }
        return builder.toString();
    }

    /**
     * Swapping map
     *
     * @param map
     * @return
     */
    private static Map<String, Character> swap(Map<Character, StringBuilder> map) {
        final Map<String, Character> originMap = new HashMap<>();
        map.forEach((k, v) -> originMap.put(v.toString(), k));
        return originMap;
    }

    /**
     * result
     *
     * @param text
     * @param map
     * @return
     * @throws IOException
     */
    private static String resultText(String text, Map<String, Character> map) throws IOException {
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length() - 1; i++) {
            for (int j = i; j < text.length(); j++) {
                final CharSequence subSequence = text.subSequence(i, j);
                if (map.containsKey(subSequence)) {
                    result.append(map.get(subSequence));
                    i = j - 1;
                    break;
                }
            }
        }
        return result.toString();
    }

    /**
     * Encoding with Hamming (7,4)
     * @param text to be encoded
     * @return encoded text
     */
    public static String encodeHamming(String text) {
        int textLength = text.length();
        String encodedText = "";

        for (int k = 0; k < textLength / 4 + 1; k++) {
            char [] dataChar;
            int [] dataBit = new int[4];
            int [] parityBit = new int[3];

            while (text.length() < 4) {
                text += 0;
            }

            dataChar = text.substring(0, 4).toCharArray();
            text = text.substring(4);

            for (int j = 0; j < 4; j++) {
                dataBit[j] = Integer.parseInt(String.valueOf(dataChar[j]));
            }

            parityBit[0] = dataBit[0] ^ dataBit[1] ^ dataBit[2];
            parityBit[1] = dataBit[1] ^ dataBit[2] ^ dataBit[3];
            parityBit[2] = dataBit[0] ^ dataBit[1] ^ dataBit[3];

            String encodingPart = "";
            for (int i : dataBit) {
                encodingPart += i;
            }
            encodedText += encodingPart;

            String parityPart = "";
            for (int i : parityBit) {
                parityPart += i;
            }
            encodedText += parityPart;
        }
        return encodedText;
    }

    /**
     * Decoding with Hamming (7,4)
     * @param text
     * @return
     */
    private static String decodeHamming(String text) {
        String decodedText = "";
        StringBuilder encodedText = new StringBuilder(text);
        String syndromePart, dataPart;

        int dataBit1, dataBit2, dataBit3, dataBit4, parityBit1, parityBit2, parityBit3, syndromeBit1, syndromeBit2, syndromeBit3;

        for(int i=0; i<encodedText.length(); i-=3)
        {
            dataBit1 = Integer.parseInt(String.valueOf(encodedText.charAt(i++)));
            dataBit2 = Integer.parseInt(String.valueOf(encodedText.charAt(i++)));
            dataBit3 = Integer.parseInt(String.valueOf(encodedText.charAt(i++)));
            dataBit4 = Integer.parseInt(String.valueOf(encodedText.charAt(i++)));

            parityBit1 = Integer.parseInt(String.valueOf(encodedText.charAt(i++)));
            parityBit2 = Integer.parseInt(String.valueOf(encodedText.charAt(i++)));
            parityBit3 = Integer.parseInt(String.valueOf(encodedText.charAt(i++)));

            syndromeBit1 = parityBit1 ^ dataBit1 ^ dataBit2 ^ dataBit3;
            syndromeBit2 = parityBit2 ^ dataBit2 ^ dataBit3 ^ dataBit4;
            syndromeBit3 = parityBit3 ^ dataBit1 ^ dataBit2 ^ dataBit4;

            syndromePart = String.valueOf(syndromeBit1)+String.valueOf(syndromeBit2)+String.valueOf(syndromeBit3);

            switch (syndromePart) {
                case "101": dataBit1 ^= 1; break;
                case "111": dataBit2 ^= 1; break;
                case "110": dataBit3 ^= 1; break;
                case "011": dataBit4 ^= 1; break;
                default:break;
            }

            dataPart = String.valueOf(dataBit1)+String.valueOf(dataBit2)+String.valueOf(dataBit3)+String.valueOf(dataBit4);
            decodedText = String.valueOf(encodedText.replace(i-7, i, dataPart));
        }

        return decodedText;
    }

    /**
     * Add noise to text encoded with Hamming
     * @param text
     * @return
     */
    private static String addNoise(String text) {
        int min = 0,
            max = 6,
            textParts = text.length() / 7 + 1,
            textLength = text.length();
        StringBuilder builder = new StringBuilder(text);

        for (int i=0;i<textParts;i++) {
            Random r = new Random();
            int position = r.nextInt((max - min) + 1) + min + i * 7;
            if (position < textLength) {
                builder.setCharAt(position, builder.charAt(position) == '0' ? '1' : '0');
            }
        }
        return builder.toString();
    }
}

